package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "POS_TRX_PR_DETAIL")
public class TrxPrDetail {
	private long id;
	private long prId;
	private long variantId;
	private int requestQty;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name = "ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PrDetail")
	@TableGenerator(name = "PrDetail", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "PrDetail", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the prId
	 */
	@Column(name = "PR_ID", nullable=false)
	public long getPrId() {
		return prId;
	}
	/**
	 * @param prId the prId to set
	 */
	public void setPrId(long prId) {
		this.prId = prId;
	}
	/**
	 * @return the variantId
	 */
	@Column(name = "VARIANT_ID", nullable=false)
	public long getVariantId() {
		return variantId;
	}
	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}
	/**
	 * @return the requestQty
	 */
	@Column(name = "REQUEST_QTY", nullable=false)
	public int getRequestQty() {
		return requestQty;
	}
	/**
	 * @param requestQty the requestQty to set
	 */
	public void setRequestQty(int requestQty) {
		this.requestQty = requestQty;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name = "CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the modifiedBy
	 */
	@Column(name = "MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
	/**
	 * @return the modifiedOn
	 */
	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	@Override
	public String toString() {
		return "TrxPrDetail [id=" + id + ", prId=" + prId + ", variantId=" + variantId + ", requestQty=" + requestQty
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + "]";
	}
	
	
}
