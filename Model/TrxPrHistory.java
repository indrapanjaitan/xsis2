package com.xsis.pos105.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table (name = "POS_TRX_PR_HISTORY")
public class TrxPrHistory {
	
	private long id;
	private long prId;
	private String status;
	private long createdBy;
	private Date createdOn;
	
	/**
	 * @return the id
	 */
	@Id
	@Column (name="ID", nullable= false)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the prId
	 */
	@Column(name="PR_ID" , nullable= false)
	public long getPrId() {
		return prId;
	}
	/**
	 * @param prId the prId to set
	 */
	public void setPrId(long prId) {
		this.prId = prId;
	}
	/**
	 * @return the status
	 */
	@Column(name="STATUS", length=20, nullable=false)
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxPrHistory [id=" + id + ", prId=" + prId + ", status=" + status + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + "]";
	}

}
