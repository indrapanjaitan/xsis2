package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="POS_TRX_SO_DETAIL")
public class TrxSoDetail {

	private long id;	
	private long soId;
	private long variantId;	
	private int qty;
	private double unitPrice;
	private double subTotal;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="SoDetail")
	@TableGenerator (name="SoDetail", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="SoDetail", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the soId
	 */
	@Column(name="SO_ID", nullable=false)
	public long getSoId() {
		return soId;
	}

	/**
	 * @param soId the soId to set
	 */
	public void setSoId(long soId) {
		this.soId = soId;
	}

	/**
	 * @return the variantId
	 */
	@Column(name="VARIANT_ID", nullable=false)
	public long getVariantId() {
		return variantId;
	}

	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	/**
	 * @return the qty
	 */
	@Column(name="QTY", nullable=false)
	public int getQty() {
		return qty;
	}

	/**
	 * @param qty the qty to set
	 */
	public void setQty(int qty) {
		this.qty = qty;
	}

	/**
	 * @return the unitPrice
	 */
	@Column(name="UNIT_PRICE", nullable=false)
	public double getUnitPrice() {
		return unitPrice;
	}

	/**
	 * @param unitPrice the unitPrice to set
	 */
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	/**
	 * @return the subTotal
	 */	
	@Column(name="SUB_TOTAL", nullable=false)
	public double getSubTotal() {
		return subTotal;
	}

	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	/**
	 * @return the createdBy
	 */	
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */	
	@Column(name="MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxSoDetail [id=" + id + ", soId=" + soId + ", variantId=" + variantId + ", qty=" + qty + ", unitPrice="
				+ unitPrice + ", subTotal=" + subTotal + ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}

	
}
