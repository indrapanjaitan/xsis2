package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "POS_TRX_PO")
public class TrxPo {
	private long id;
	private long prId;
	private long outletId;
	private long supplierId;
	private String poNo;
	private String notes;
	private double grandTotal;
	private String status;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name = "ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "Po")
	@TableGenerator(name = "Po", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "Po", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the prId
	 */
	@Column(name = "PR_ID", nullable=false)
	public long getPrId() {
		return prId;
	}
	/**
	 * @param prId the prId to set
	 */
	public void setPrId(long prId) {
		this.prId = prId;
	}
	/**
	 * @return the outletId
	 */
	@Column(name = "OUTLET_ID", nullable=false)
	public long getOutletId() {
		return outletId;
	}
	/**
	 * @param outletId the outletId to set
	 */
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	/**
	 * @return the supplierId
	 */
	@Column(name = "SUPPLIER_ID", nullable=false)
	public long getSupplierId() {
		return supplierId;
	}
	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}
	/**
	 * @return the poNo
	 */
	@Column(name = "PO_NO", length=20, nullable=false)
	public String getPoNo() {
		return poNo;
	}
	/**
	 * @param poNo the poNo to set
	 */
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	/**
	 * @return the notes
	 */
	@Column(name = "NOTES", length=255)
	public String getNotes() {
		return notes;
	}
	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}
	/**
	 * @return the grandTotal
	 */
	@Column(name = "GRAND_TOTAL", nullable=false)
	public double getGrandTotal() {
		return grandTotal;
	}
	/**
	 * @param grandTotal the grandTotal to set
	 */
	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}
	/**
	 * @return the status
	 */
	@Column(name = "STATUS", length=20, nullable=false)
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name = "CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the modifiedBy
	 */
	@Column(name = "MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedOn
	 */
	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxPo [id=" + id + ", prId=" + prId + ", outletId=" + outletId + ", supplierId=" + supplierId
				+ ", poNo=" + poNo + ", notes=" + notes + ", grandTotal=" + grandTotal + ", status=" + status
				+ ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + "]";
	}
	

}
