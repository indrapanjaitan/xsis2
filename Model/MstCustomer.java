package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.Type;

@Entity
@Table(name="POS_MST_CUSTOMER")
public class MstCustomer {

	private long id;
	private String name;
	private String email;
	private String phone;
	private Date dob;
	private String address;
	private long provinceId;
	private long regionId;
	private long districtId;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	private boolean active;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "Customer")
	@TableGenerator(name = "Customer", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "Customer", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	@Column(name="NAME", length=50, nullable=false)
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	@Column(name="EMAIL", length=50, nullable=false)
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the phone
	 */
	@Column(name="PHONE", length=16)
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the dob
	 */
	@Column(name="DOB")
	public Date getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}
	/**
	 * @return the address
	 */
	@Column(name="ADDRESS", length=255)
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the provinceId
	 */
	@Column(name="PROVINCE_ID", nullable=false)
	public long getProvinceId() {
		return provinceId;
	}
	/**
	 * @param provinceId the provinceId to set
	 */
	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}
	/**
	 * @return the regionId
	 */
	@Column(name="REGION_ID", nullable=false)
	public long getRegionId() {
		return regionId;
	}
	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return the districtId
	 */
	@Column(name="DISTRICT_ID", nullable=false)
	public long getDistrictId() {
		return districtId;
	}
	/**
	 * @param districtId the districtId to set
	 */
	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the modifiedBy
	 */
	@Column(name="MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedOn
	 */
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/**
	 * @return the active
	 */
	@Column(name="ACTIVE", nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MstCustomer [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + ", dob=" + dob
				+ ", address=" + address + ", provinceId=" + provinceId + ", regionId=" + regionId + ", districtId="
				+ districtId + ", createdBy=" + createdBy + ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy
				+ ", modifiedOn=" + modifiedOn + ", active=" + active + "]";
	}

	
}
