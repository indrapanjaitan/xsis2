package com.xsis.pos105.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name = "POS_TRX_TRANSFER_STOCK")
public class TrxTransferStock {
	
	private int id;
	private long fromOutlet;
	private long toOutlet;
	private String notes;
	private String status;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;

	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=true)
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the fromOutlet
	 */
	@Column(name="FROM_OUTLET", nullable=false)
	public long getFromOutlet() {
		return fromOutlet;
	}

	/**
	 * @param fromOutlet the fromOutlet to set
	 */
	public void setFromOutlet(long fromOutlet) {
		this.fromOutlet = fromOutlet;
	}

	/**
	 * @return the toOutlet
	 */
	@Column(name="TO_OUTLET", nullable= false)
	public long getToOutlet() {
		return toOutlet;
	}

	/**
	 * @param toOutlet the toOutlet to set
	 */
	public void setToOutlet(long toOutlet) {
		this.toOutlet = toOutlet;
	}

	/**
	 * @return the notes
	 */
	@Column(name="NOTES", length=255)
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the status
	 */
	@Column(name="STATUS", nullable=false)
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the createdBy
	 */
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	@Column(name="MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxTransferStock [id=" + id + ", fromOutlet=" + fromOutlet + ", toOutlet=" + toOutlet + ", notes="
				+ notes + ", status=" + status + ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}

}
