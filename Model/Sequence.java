package com.xsis.pos105.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_SEQUENCE_105")
public class Sequence {
	private String id;
	private Integer nomor;

	@Id
	@Column(name = "SEQ_NAME")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "SEQ_VAL")
	public Integer getNomor() {
		return nomor;
	}

	public void setNomor(Integer nomor) {
		this.nomor = nomor;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sequence [id=" + id + ", nomor=" + nomor + "]";
	}
	

}
