package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name = "POS_TRX_PO_DETAIL")
public class TrxPoDetail {
	private long id;
	private long poId;
	private long variantId;
	private int requestQty;
	private double unitCost;
	private double subTotal;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name = "ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "PoDetail")
	@TableGenerator(name = "PoDetail", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "PoDetail", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the poId
	 */
	@Column(name = "PO_ID")
	public long getPoId() {
		return poId;
	}
	/**
	 * @param poId the poId to set
	 */
	public void setPoId(long poId) {
		this.poId = poId;
	}
	/**
	 * @return the variantId
	 */
	@Column(name = "VARIANT_ID", nullable=false)
	public long getVariantId() {
		return variantId;
	}
	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}
	/**
	 * @return the requestQty
	 */
	@Column(name = "REQUEST_QTY", nullable=false)
	public int getRequestQty() {
		return requestQty;
	}
	/**
	 * @param requestQty the requestQty to set
	 */
	public void setRequestQty(int requestQty) {
		this.requestQty = requestQty;
	}
	/**
	 * @return the unitCost
	 */
	@Column(name = "UNIT_COST")
	public double getUnitCost() {
		return unitCost;
	}
	/**
	 * @param unitCost the unitCost to set
	 */
	public void setUnitCost(double unitCost) {
		this.unitCost = unitCost;
	}
	/**
	 * @return the subTotal
	 */
	@Column(name = "SUB_TOTAL")
	public double getSubTotal() {
		return subTotal;
	}
	/**
	 * @param subTotal the subTotal to set
	 */
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name = "CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the modifiedBy
	 */
	@Column(name = "MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedOn
	 */
	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxPoDetail [id=" + id + ", poId=" + poId + ", variantId=" + variantId + ", requestQty=" + requestQty
				+ ", unitCost=" + unitCost + ", subTotal=" + subTotal + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}
	
	
}
