package com.xsis.pos105.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="POS_MST_EMPLOYEE_OUTLET")
public class MstEmployeeOutlet {
	private long id;
	private long employeedId;
	private long outletId;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "EmployeeOutlet")
	@TableGenerator(name = "EmployeeOutlet", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "EmployeeOutlet", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the employeedId
	 */
	@Column(name="EMPLOYEE_ID",  nullable=false)
	public long getEmployeedId() {
		return employeedId;
	}
	/**
	 * @param employeedId the employeedId to set
	 */
	public void setEmployeedId(long employeedId) {
		this.employeedId = employeedId;
	}
	/**
	 * @return the outletId
	 */
	@Column(name="OUTLET_ID",  nullable=false)
	public long getOutletId() {
		return outletId;
	}
	/**
	 * @param outletId the outletId to set
	 */
	public void setOutletId(long outletId) {
		this.outletId = outletId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmployeeOutlet [id=" + id + ", employeedId=" + employeedId + ", outletId=" + outletId + "]";
	}
	

}
