package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="POS_TRX_ADJUSTMENT_DETAIL")
public class TrxAdjustmentDetail {

	private long id;	
	private long adjustmentId;	
	private long variantId;	
	private int inStock;
	private int actualStock;
	private long createdBy;
	private Date createdOn;	
	private long modifiedBy;	
	private Date modifiedOn;

	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="AdjustmentDetail")
	@TableGenerator (name="AdjustmentDetail", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="AdjustmentDetail", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the adjustmentId
	 */
	@Column(name="ADJUSTMENT_ID", nullable=false)
	public long getAdjustmentId() {
		return adjustmentId;
	}

	/**
	 * @param adjustmentId the adjustmentId to set
	 */
	public void setAdjustmentId(long adjustmentId) {
		this.adjustmentId = adjustmentId;
	}

	/**
	 * @return the variantId
	 */
	@Column(name="VARIANT_ID", nullable=false)
	public long getVariantId() {
		return variantId;
	}

	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	/**
	 * @return the inStock
	 */
	@Column(name="IN_STOCK", nullable=false)
	public int getInStock() {
		return inStock;
	}

	/**
	 * @param inStock the inStock to set
	 */
	public void setInStock(int inStock) {
		this.inStock = inStock;
	}

	/**
	 * @return the actualStock
	 */	
	@Column(name="ACTUAL_STOCK", nullable=false)
	public int getActualStock() {
		return actualStock;
	}

	/**
	 * @param actualStock the actualStock to set
	 */
	public void setActualStock(int actualStock) {
		this.actualStock = actualStock;
	}

	/**
	 * @return the createdBy
	 */	
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	@Column(name="MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxAdjustmentDetail [id=" + id + ", adjustmentId=" + adjustmentId + ", variantId=" + variantId
				+ ", inStock=" + inStock + ", actualStock=" + actualStock + ", createdBy=" + createdBy + ", createdOn="
				+ createdOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}
	
		
}
