package com.xsis.pos105.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="POS_TRX_TRANSFER_STOCK_DETAIL")
public class TrxTransferStockDetail {
	
	private long ID;
	private long transferId;
	private int instock;
	private long variantId;
	private int transferQty;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;

	
	/**
	 * @return the iD
	 */
	@Id
	@Column(name = "ID", nullable=false)
	public long getID() {
		return ID;
	}

	/**
	 * @param iD the iD to set
	 */
	public void setID(long iD) {
		ID = iD;
	}
	/**
	 * @return the transferId
	 */
	@Column(name="TRANSFER_ID", nullable=false)
	public long getTransferId() {
		return transferId;
	}
	
	/**
	 * @param transferId the transferId to set
	 */
	
	public void setTransferId(long transferId) {
		this.transferId = transferId;
	}

	/**
	 * @return the instock
	 */
	@Column(name="INSTOCK")
	public int getInstock() {
		return instock;
	}

	/**
	 * @param instock the instock to set
	 */
	public void setInstock(int instock) {
		this.instock = instock;
	}

	/**
	 * @return the variantId
	 */
	@Column(name="VARIANT_ID", nullable=false)
	public long getVariantId() {
		return variantId;
	}

	/**
	 * @param variantId the variantId to set
	 */
	public void setVariantId(long variantId) {
		this.variantId = variantId;
	}

	/**
	 * @return the transferQty
	 */
	@Column(name="TRANSFER_QTY", nullable=false)
	public int getTransferQty() {
		return transferQty;
	}

	/**
	 * @param transferQty the transferQty to set
	 */
	public void setTransferQty(int transferQty) {
		this.transferQty = transferQty;
	}

	/**
	 * @return the createdBy
	 */
	@Column(name = "CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Column(name = "CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the modifiedBy
	 */
	@Column(name = "MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedOn
	 */
	@Column(name = "MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TrxTransferStockDetail [ID=" + ID + ", transferId=" + transferId + ", instock=" + instock
				+ ", variantId=" + variantId + ", transferQty=" + transferQty + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}

}
