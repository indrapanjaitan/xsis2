package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import org.hibernate.annotations.Type;

@Entity
@Table(name="POS_MST_DISTRICT")
public class MstDistrict {

	private long id;
	private long regionId;
	private String name;
	private long createdBy;
	private Date createdOn;
	private long modifiedBy;
	private Date modifiedOn;
	private boolean active;
	
	/**
	 * @return the id
	 */
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "District")
	@TableGenerator(name = "District", table = "T_SEQUENCE_105", pkColumnName = "SEQ_NAME", pkColumnValue = "District", valueColumnName = "SEQ_VAL", allocationSize = 1, initialValue = 1)
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the regionId
	 */
	@Column(name="REGION_ID", nullable=false)
	public long getRegionId() {
		return regionId;
	}
	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return the name
	 */
	@Column(name="NAME", length=50, nullable=false)
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the createdBy
	 */
	@Column(name="CREATED_BY")
	public long getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column(name="CREATED_ON")
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the modifiedBy
	 */
	@Column(name="MODIFIED_BY")
	public long getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedOn
	 */
	@Column(name="MODIFIED_ON")
	public Date getModifiedOn() {
		return modifiedOn;
	}
	/**
	 * @param modifiedOn the modifiedOn to set
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	/**
	 * @return the active
	 */
	@Column(name="ACTIVE", nullable=false)
	@Type(type = "org.hibernate.type.NumericBooleanType")
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MstDistrict [id=" + id + ", regionId=" + regionId + ", name=" + name + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + ", active="
				+ active + "]";
	}
	

}
