package com.xsis.pos105.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.ContohDao;
import com.xsis.pos105.service.ContohService;
import com.xsis.pos105.viewmodel.ContohView;

@Transactional
@Service
public class ContohServiceImpl implements ContohService{
	
	@Autowired
	private ContohDao contohDao;

	@Override
	public Collection<ContohView> getDataViewMahasiswa() throws Exception {
		// TODO Auto-generated method stub
		return contohDao.getDataViewMahasiswa();
	}

}
