package com.xsis.pos105.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.TrxPoDao;
import com.xsis.pos105.model.TrxPo;
import com.xsis.pos105.service.TrxPoService;

@Service
@Transactional
public class TrxPoServiceImpl implements TrxPoService{
		
	@Autowired
	private TrxPoDao trxPoDao;

	@Override
	public Collection<TrxPo> getAll() throws Exception {
		return trxPoDao.getAll();
	}

	@Override
	public TrxPo getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return trxPoDao.getById(id);
	}

	@Override
	public void save(TrxPo trxPo) throws Exception {
		// TODO Auto-generated method stub
		trxPoDao.save(trxPo);
	}

	@Override
	public void update(TrxPo trxPo) throws Exception {
		// TODO Auto-generated method stub
		trxPoDao.update(trxPo);
	}

	@Override
	public void delete(TrxPo trxPo) throws Exception {
		// TODO Auto-generated method stub
		trxPoDao.delete(trxPo);
	}
	
	
}
