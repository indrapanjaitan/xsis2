package com.xsis.pos105.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xsis.pos105.dao.TrxPoHistoryDao;
import com.xsis.pos105.model.TrxPoHistory;
import com.xsis.pos105.service.TrxPoHistoryService;

@Service
@Transactional
public class TrxPoHistoryServiceImpl implements TrxPoHistoryService{
	@Autowired
	private TrxPoHistoryDao trxPoHistoryDao;
	@Override
	public Collection<TrxPoHistory> getAll() throws Exception {
		// TODO Auto-generated method stub
		return trxPoHistoryDao.getAll();
	}

	@Override
	public TrxPoHistory getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return trxPoHistoryDao.getById(id);
	}

	@Override
	public void save(TrxPoHistory trxPoHistory) throws Exception {
		// TODO Auto-generated method stub
		trxPoHistoryDao.save(trxPoHistory);
	}

	@Override
	public void update(TrxPoHistory trxPoHistory) throws Exception {
		// TODO Auto-generated method stub
		trxPoHistoryDao.update(trxPoHistory);
	}

	@Override
	public void delete(TrxPoHistory trxPoHistory) throws Exception {
		// TODO Auto-generated method stub
		trxPoHistoryDao.delete(trxPoHistory);
	}

}
