package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.viewmodel.ContohView;

public interface ContohService {
	
	public Collection<ContohView> getDataViewMahasiswa()throws Exception;

}
