package com.xsis.pos105.service;

import java.util.Collection;

import com.xsis.pos105.model.MstSupplier;

public interface MstSupplierService {

	public Collection<MstSupplier> getAll() throws Exception;
	public MstSupplier getById(int id) throws Exception;
	public void save(MstSupplier mstSupplier) throws Exception;
	public void update(MstSupplier mstSupplier) throws Exception;
	public void delete(MstSupplier mstSupplier) throws Exception;
	
}
