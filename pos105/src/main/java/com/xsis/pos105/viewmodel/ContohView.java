package com.xsis.pos105.viewmodel;

public class ContohView {
	private int idMahasiswa;
	private String namaMahasiswa;
	private String alamatMahasiswa;
	private String namaDosen;
	public int getIdMahasiswa() {
		return idMahasiswa;
	}
	public void setIdMahasiswa(int idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}
	public String getNamaMahasiswa() {
		return namaMahasiswa;
	}
	public void setNamaMahasiswa(String namaMahasiswa) {
		this.namaMahasiswa = namaMahasiswa;
	}
	public String getAlamatMahasiswa() {
		return alamatMahasiswa;
	}
	public void setAlamatMahasiswa(String alamatMahasiswa) {
		this.alamatMahasiswa = alamatMahasiswa;
	}
	public String getNamaDosen() {
		return namaDosen;
	}
	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}
	
	

}
