package com.xsis.pos105.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.MstSupplierDao;
import com.xsis.pos105.model.MstSupplier;

@Repository
public class MstSupplierDaoImpl implements MstSupplierDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Collection<MstSupplier> getAll() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Supplier";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public MstSupplier getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Supplier where id=:id";
		Query query = session.createQuery(hql);
		query.setInteger("id", id);
		return (MstSupplier) query.list().get(0);
	}

	@Override
	public void save(MstSupplier mstSupplier) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(mstSupplier);
	}

	@Override
	public void update(MstSupplier mstSupplier) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(mstSupplier);
	}

	@Override
	public void delete(MstSupplier mstSupplier) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(mstSupplier);
	}

	
}
