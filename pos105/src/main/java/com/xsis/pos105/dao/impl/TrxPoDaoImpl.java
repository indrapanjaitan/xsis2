package com.xsis.pos105.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.TrxPoDao;
import com.xsis.pos105.model.TrxPo;

@Repository
public class TrxPoDaoImpl implements TrxPoDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<TrxPo> getAll() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPo";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public TrxPo getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPo where id=:id";
		Query query = session.createQuery(hql);
		return (TrxPo) query.list().get(0);
	}

	@Override
	public void save(TrxPo trxPo) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(trxPo);
	}

	@Override
	public void update(TrxPo trxPo) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(trxPo);
	}

	@Override
	public void delete(TrxPo trxPo) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(trxPo);
	}

}
