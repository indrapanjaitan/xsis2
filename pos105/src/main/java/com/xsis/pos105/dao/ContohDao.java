package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.viewmodel.ContohView;

public interface ContohDao {
	
	public Collection<ContohView> getDataViewMahasiswa()throws Exception;

}
