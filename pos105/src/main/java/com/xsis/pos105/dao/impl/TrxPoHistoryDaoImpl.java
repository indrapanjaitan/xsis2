package com.xsis.pos105.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.TrxPoHistoryDao;
import com.xsis.pos105.model.TrxPoHistory;

@Repository
public class TrxPoHistoryDaoImpl implements TrxPoHistoryDao{
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<TrxPoHistory> getAll() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPoHistory";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public TrxPoHistory getById(int id) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPoHistory where id=:id";
		Query query = session.createQuery(hql);
		return (TrxPoHistory) query.list().get(0);
	}

	@Override
	public void save(TrxPoHistory trxPoHistory) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.save(trxPoHistory);
	}

	@Override
	public void update(TrxPoHistory trxPoHistory) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.update(trxPoHistory);
	}

	@Override
	public void delete(TrxPoHistory trxPoHistory) throws Exception {
		Session session = sessionFactory.getCurrentSession();
		session.delete(trxPoHistory);
	}

}
