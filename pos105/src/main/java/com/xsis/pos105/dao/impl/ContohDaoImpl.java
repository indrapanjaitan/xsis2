package com.xsis.pos105.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.ContohDao;
import com.xsis.pos105.viewmodel.ContohView;

@Repository
public class ContohDaoImpl implements ContohDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Collection<ContohView> getDataViewMahasiswa() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		StringBuilder query = new StringBuilder("");
		query.append("select m.ID_MAHASISWA, m.NAMA_MAHASISWA, m.ALAMAT_MAHASISWA, d.NAMA_DOSEN from contoh_mahasiswa m ");
		query.append("inner join contoh_dosen d on m.ID_DOSEN = d.ID_DOSEN ");
		
		MyWork work = new MyWork(query.toString());
		session.doWork(work);
		PreparedStatement ps = work.getPreparedStatement();
		ResultSet rs = ps.executeQuery();
		
		Collection<ContohView> result = new ArrayList<ContohView>();
		while (rs.next()) {
			ContohView cv = new ContohView();
			cv.setIdMahasiswa(rs.getInt("ID_MAHASISWA"));
			cv.setNamaDosen(rs.getString("NAMA_DOSEN"));
			cv.setNamaMahasiswa(rs.getString("NAMA_MAHASISWA"));
			cv.setAlamatMahasiswa(rs.getString("ALAMAT_MAHASISWA"));
			result.add(cv);
		}
		return result;
	}
	
	
	class MyWork implements Work {
		private String query;
		private PreparedStatement ps;

		public MyWork(String paramString) {
			this.query = paramString;
		}

		public void execute(Connection con) throws SQLException {
			this.ps = con.prepareStatement(this.query, 1004, 1007);
		}

		public PreparedStatement getPreparedStatement() {
			return this.ps;
		}
	}
	
}
