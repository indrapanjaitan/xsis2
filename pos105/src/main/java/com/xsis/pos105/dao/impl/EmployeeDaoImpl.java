package com.xsis.pos105.dao.impl;

import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

//import com.xsis.bootcamp.model.MstEmployeeModel;
import com.xsis.pos105.dao.EmployeeDao;
//import com.xsis.pos105.dao.MstEmployeeViewModel;
import com.xsis.pos105.model.MstEmployee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao{


	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<MstEmployee> get() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<MstEmployee> result = session.createQuery("from MstEmployee").list();
		return result;
	}

	@Override
	public List<MstEmployee> search(String keySearch) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(MstEmployee.class);
		criteria.add(Restrictions.like("name", "%" + keySearch + "%"));
		List<MstEmployee> result = criteria.list();
		return result;
	}

	@Override
	public MstEmployee getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		MstEmployee result = session.get(MstEmployee.class, id);
		return result;
	}

//	@Override
//	public MstEmployee getByEmail(String email) throws Exception {
//		// TODO Auto-generated method stub
//		Session session = sessionFactory.getCurrentSession();
//		Query query = session.createQuery("from MstEmployee x where x.email = :email");
//		query.setParameter("email", email);
//		MstEmployee result = (MstEmployee)query.getSingleResult();
//		return result;
//	}
	
//	@Override
//	public void insert(MstEmployeeViewModel model) throws Exception {
//		// TODO Auto-generated method stub
//		Session session = sessionFactory.getCurrentSession();
//		// employee
//		MstEmployeeModel emp = new MstEmployeeModel();
//		emp.setFirstName(model.getFirstName());
//		emp.setMiddleName(model.getMiddleName());
//		emp.setLastName(model.getLastName());
//		emp.setEmail(model.getEmail());
//		emp.setPhone(model.getPhone());
//		emp.setTitle(model.getTitle());		
//		emp.setDateOfBirth(model.getDateOfBirth());
//		emp.setHaveAccount(model.getHaveAccount());
//		emp.setProvinceId(model.getProvinceId());
//		emp.setRegionId(model.getRegionId());
//		emp.setDistrictId(model.getDistrictId());
//		emp.setVillageId(model.getVillageId());
//		emp.setAddress(model.getAddress());
//		emp.setCreatedBy(model.getCreatedBy());
//		emp.setCreatedOn(model.getCreatedOn());
//		emp.setModifiedBy(model.getModifiedBy());
//		emp.setModifiedOn(model.getModifiedOn());
//		emp.setActive(1);
//		session.save(emp);
//		
//		// outlet
//		List<Integer> outletList = model.getOutletId();
//		if(outletList != null && outletList.size() > 0){
//			for (Integer item : outletList) {
//				MstEmployeeOutletModel outletEmp = new MstEmployeeOutletModel();
//				outletEmp.setEmployeeId(emp.getId());
//				outletEmp.setOutletId(item);
//				session.save(outletEmp);
//			}
//		}
//		
//		// save ke user
//		if(model.getRoleId() > 0 && !model.getUserName().equals("") && !model.getPassword().equals("")){
//			MstUserModel user = new MstUserModel();
//			user.setUsername(model.getUserName());
//			user.setPassword(model.getPassword());
//			user.setEmployeeId(emp.getId());
//			user.setRoleId(model.getRoleId());
//			user.setCreatedBy(model.getCreatedBy());
//			user.setCreatedOn(model.getCreatedOn());
//			user.setModifiedBy(model.getModifiedBy());
//			user.setModifiedOn(model.getModifiedOn());
//			user.setActive(1);
//			session.save(user);
//		}
//	}

	@Override
	public void update(MstEmployee model) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(MstEmployee model) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public MstEmployee getByEmail(String email) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}



	
}
