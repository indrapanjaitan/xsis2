package com.xsis.pos105.dao;

import java.util.Collection;
import com.xsis.pos105.model.TrxPr;

public interface TrxPrDao {
	public Collection<TrxPr> getAll() throws Exception;
	public TrxPr getById(int id) throws Exception;
	public void save(TrxPr pr) throws Exception;
	public void update(TrxPr pr) throws Exception;
	public void delete(TrxPr pr) throws Exception;
}
