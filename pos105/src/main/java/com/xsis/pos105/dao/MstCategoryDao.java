package com.xsis.pos105.dao;

import java.util.Collection;

import com.xsis.pos105.model.MstCategory;

public interface MstCategoryDao {
	public Collection<MstCategory> getAll() throws Exception;

	public MstCategory getById(long id) throws Exception;

	public void save(MstCategory MstCategory) throws Exception;

	public void update(MstCategory MstCategory) throws Exception;

	public void delete(MstCategory MstCategory) throws Exception;
}
