package com.xsis.pos105.dao.impl;

import java.util.Collection;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.xsis.pos105.dao.TrxPrDao;
import com.xsis.pos105.model.TrxPr;
import com.xsis.pos105.model.TrxTransferStock;

@Repository
public class TrxPrDaoImpl implements TrxPrDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Collection<TrxPr> getAll() throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPr";
		Query query = session.createQuery(hql);
		return query.list();
	}

	@Override
	public TrxPr getById(int id) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		String hql = "from TrxPr where id=:id";
		Query query = session.createQuery(hql);
		return (TrxPr) query.list().get(0);
	}

	@Override
	public void save(TrxPr trxPr) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(trxPr);
	}

	@Override
	public void update(TrxPr trxPr) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.update(trxPr);
	}

	@Override
	public void delete(TrxPr trxPr) throws Exception {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.delete(trxPr);
	}

}
