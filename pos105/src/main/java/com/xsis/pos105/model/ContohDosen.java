package com.xsis.pos105.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="CONTOH_DOSEN")
public class ContohDosen {
	
	private int idDosen;
	private String namaDosen;
	
	@Id
	@Column(name="ID_DOSEN", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="Dosen")
	@TableGenerator (name="Dosen", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="Dosen", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	public int getIdDosen() {
		return idDosen;
	}
	public void setIdDosen(int idDosen) {
		this.idDosen = idDosen;
	}
	
	@Column(name="NAMA_DOSEN")
	public String getNamaDosen() {
		return namaDosen;
	}
	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}
	
	

}
