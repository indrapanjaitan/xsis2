package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="POS_TRX_SO")
public class TrxSo {

	@Id
	@Column(name="ID")
	@GeneratedValue (strategy=GenerationType.TABLE, generator="So")
	@TableGenerator (name="So", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="So", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	private long id;
	
	@Column(name="CUSTOMER_ID", nullable=false)
	private long customerId;
	
	@Column(name="GRAND_TOTAL", nullable=false)
	private double grandTotal;
	
	
	@Column(name="CREATED_BY", nullable=true)
	private long createdBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=true)
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", nullable=true)
	private long modifiedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public String toString() {
		return "So [id=" + id + ", customerId=" + customerId + ", grandTotal=" + grandTotal + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", modifiedBy=" + modifiedBy + ", modifiedOn=" + modifiedOn + "]";
	}
	
	
}
