package com.xsis.pos105.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="CONTOH_MAHASISWA")
public class ContohMahasiswa {
	
	private int idMahasiswa;
	private String namaMahasiswa;
	private String alamatMahasiswa;
	private int idDosen;
	
	@Id
	@Column(name="ID_MAHASISWA", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="Mahasiswa")
	@TableGenerator (name="Mahasiswa", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="Mahasiswa", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	public int getIdMahasiswa() {
		return idMahasiswa;
	}
	public void setIdMahasiswa(int idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}
	
	@Column(name="NAMA_MAHASISWA")
	public String getNamaMahasiswa() {
		return namaMahasiswa;
	}
	public void setNamaMahasiswa(String namaMahasiswa) {
		this.namaMahasiswa = namaMahasiswa;
	}
	
	@Column(name="ALAMAT_MAHASISWA")
	public String getAlamatMahasiswa() {
		return alamatMahasiswa;
	}
	public void setAlamatMahasiswa(String alamatMahasiswa) {
		this.alamatMahasiswa = alamatMahasiswa;
	}
	
	@Column(name="ID_DOSEN")
	public int getIdDosen() {
		return idDosen;
	}
	public void setIdDosen(int idDosen) {
		this.idDosen = idDosen;
	}
	
	

}
