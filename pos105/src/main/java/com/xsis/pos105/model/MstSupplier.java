package com.xsis.pos105.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="POS_MST_SUPPLIER")
public class MstSupplier {
	
	@Id
	@Column(name="ID", nullable=false)
	@GeneratedValue (strategy=GenerationType.TABLE, generator="Supplier")
	@TableGenerator (name="Supplier", table="T_SEQUENCE_105", pkColumnName="SEQ_NAME", pkColumnValue="Supplier", valueColumnName="SEQ_VAL", allocationSize=1, initialValue=1)
	private long id;
	
	@Column(name="NAME", nullable=false, length=50)
	private String name;
	
	@Column(name="ADDRESS", nullable=true, length=255)
	private String address;
	
	@Column(name="PHONE", nullable=true)
	private String phone;
	
	@Column(name="EMAIL", nullable=true)
	private String email;
	
	@Column(name="PROVINCE_ID", nullable=false)
	private long provinceId;
	
	@Column(name="REGION_ID", nullable=false, length=6)
	private long regionId;
	
	@Column(name="DISTRICT_ID", nullable=false, length=16)
	private long districtId;
	
	@Column(name="POSTAL_CODE", nullable=true, length=60)
	private String postalCode;
	
	@Column(name="CREATED_BY", nullable=true)
	private long createdBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_ON", nullable=true)
	private Date createdOn;
	
	@Column(name="MODIFIED_BY", nullable=true)
	private long modifiedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MODIFIED_ON", nullable=true)
	private Date modifiedOn;
	
	@Column(name="active", nullable=false)
	private boolean active;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(long provinceId) {
		this.provinceId = provinceId;
	}

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(long districtId) {
		this.districtId = districtId;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	
	
}
