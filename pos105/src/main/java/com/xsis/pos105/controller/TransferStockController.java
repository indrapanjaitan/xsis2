package com.xsis.pos105.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TransferStockController {

	@RequestMapping(value="/transferStock", method=RequestMethod.GET)
	public String index() {
		return "trx_transfer_stock/index";
	}
}
