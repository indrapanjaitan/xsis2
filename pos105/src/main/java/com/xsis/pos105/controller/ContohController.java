package com.xsis.pos105.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.pos105.service.ContohService;

@Controller
public class ContohController {

	private Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private ContohService contohService;
	
	@RequestMapping("/contohData")
	public String getMahasiswaDosen(Model model){
		try {
			model.addAttribute("dataMahasiswaDosen",contohService.getDataViewMahasiswa());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return null;
	}

}
