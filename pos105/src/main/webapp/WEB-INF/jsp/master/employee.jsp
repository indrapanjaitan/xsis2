<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<!-- modal -->
<div id="modal-form" class="modal" tabindex="-1" role="dialog">
<div>
			<div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
</div>

<!-- list -->
<div class="box box-info">
	<div class="box-header">
		<h3 class="box-title">Employee List</h3>
	</div>
	<div class="box-body">
	<table class= "table table-responsive table-bordered"> 
	<thead>
		<tr>
			<td>Name</td>
			<td>Email</td>
			<td>Have Account</td>
		</tr>
	</thead>
		<tbody id="list-data"></tbody>
	</table>
	</div>
</div>

<script>
	function loadData(){
		$.ajax({
			url:'${contextName}/master/employee/list',
			dataType:'html',
			type:'get',
			success:function(result){
				$("#list-data").html(result);
			}
		})
	}
	$(document).ready(function(){
		//panggil aku apa, panggil method load data
		loadData()
	})
</script>

</body>
</html>